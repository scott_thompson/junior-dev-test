# Task Countdown Page

## Steps

1. Fork or download this project
2. Open the PSD file named `countdown.psd` located in the `design` folder
3. Ignore all the fonts used in the PSD and make sure you don't provide any non open sourced fonts
4. Implement the design using a frontend Library or Framework
5. Add instructions on how to build
6. Send us the source code of the finished project

## Required implementation

- Countdown should point to 15/03/2019 17:00 GMT + 1
- Join and Login buttons should point to http://square-enix-games.com
- The calendar button should add an event to a google calendar
- Newsletter sign up should link to https://square-enix-games.com/

## Extra points

- Usage server side rendering
- Page refreshes when countdown reaches 00:00:00:00
- Page interactions
- Usage of git and often commits
- Snapshots using jest
